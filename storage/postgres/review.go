package postgres

import (
	"fmt"
	pb "review_service/genproto/review"

	"github.com/jmoiron/sqlx"
)

type reviewRepo struct {
	db *sqlx.DB
}

func NewReviewRepo(db *sqlx.DB) *reviewRepo {
	return &reviewRepo{db: db}
}

// CreateReview ...
func (r *reviewRepo) CreateReview(review *pb.ReviewRequest) (*pb.ReviewResponse, error) {
	reviewResp := pb.ReviewResponse{}
	err := r.db.QueryRow(`
	insert into review (
		post_id,
		owner_id,
		name,
		rating,
		description
		) 
	values ($1,$2,$3,$4,$5) 
		returning 
		id, post_id, owner_id, name, rating, description, created_at`,
		review.PostId, review.OwnerId, review.Name, review.Rating, review.Description,
	).Scan(
		&reviewResp.Id,
		&reviewResp.PostId,
		&reviewResp.OwnerId,
		&reviewResp.Name,
		&reviewResp.Rating,
		&reviewResp.Description,
		&reviewResp.CreatedAt,
	)
	if err != nil {
		return &pb.ReviewResponse{}, err
	}
	return &reviewResp, nil
}

// GetReviewById ...
func (r *reviewRepo) GetReviewById(req *pb.ReviewId) (*pb.Reviews, error) {
	response := pb.Reviews{}
	rows, err := r.db.Query(`SELECT 
			id, post_id, owner_id, 
			name, rating, 
			description,
			created_at,
			updated_at
			FROM review 
			WHERE id = $1 AND deleted_at IS NULL`, req.Id)
	if err != nil {
		return &pb.Reviews{}, err
	}
	for rows.Next() {
		reviewRes := pb.ReviewResponse{}
		err = rows.Scan(
			&reviewRes.Id,
			&reviewRes.PostId,
			&reviewRes.OwnerId,
			&reviewRes.Name,
			&reviewRes.Rating,
			&reviewRes.Description,
			&reviewRes.CreatedAt,
			&reviewRes.UdpatedAt,
		)
		if err != nil {
			return &pb.Reviews{}, err
		}
		response.Reviews = append(response.Reviews, &reviewRes)
	}
	return &response, nil
}

// GetReviewPost ...
func (r *reviewRepo) GetReviewPost(postId int64) (*pb.Reviews, error) {
	rows, err := r.db.Query(`SELECT 
		id, post_id, 
		owner_id, name, 
		rating, description
		FROM  review WHERE post_id=$1 and deleted_at IS NULL`, postId)
	if err != nil {
		fmt.Println("error while getting review with postId")
		return &pb.Reviews{}, err
	}
	defer rows.Close()
	reviews := pb.Reviews{}
	for rows.Next() {
		review := pb.ReviewResponse{}
		err = rows.Scan(&review.Id,
			&review.PostId,
			&review.OwnerId,
			&review.Name,
			&review.Rating,
			&review.Description)
		if err != nil {
			fmt.Println("error while scanning review with postId")
			return &pb.Reviews{}, err
		}
		reviews.Reviews = append(reviews.Reviews, &review)
	}
	return &reviews, nil
}

// GetReviewCustomer ...
func (r *reviewRepo) GetReviewCustomer(ownerId int64) (*pb.Reviews, error) {
	rows, err := r.db.Query(`SELECT 
		id, post_id, 
		owner_id, name, 
		rating, description
		FROM  review WHERE owner_id=$1 and deleted_at IS NULL`, ownerId)
	if err != nil {
		fmt.Println("error while getting review with ownerId")
		return &pb.Reviews{}, err
	}
	defer rows.Close()
	reviews := pb.Reviews{}
	for rows.Next() {
		review := pb.ReviewResponse{}
		err = rows.Scan(&review.Id,
			&review.PostId,
			&review.OwnerId,
			&review.Name,
			&review.Rating,
			&review.Description)
		if err != nil {
			fmt.Println("error while scanning review with ownerId")
			return &pb.Reviews{}, err
		}
		reviews.Reviews = append(reviews.Reviews, &review)
	}
	return &reviews, nil
}

// UpdateReview ...
func (r *reviewRepo) UpdateReview(req *pb.ReviewUp) (*pb.ReviewResponse, error) {
	review := pb.ReviewResponse{}
	_, err := r.db.Exec(`update review set 
			name=$1,
			description=$2,
			rating=$3,updated_at=NOW()
			where id = $4 AND deleted_at IS NULL`,
		req.Name, req.Description, req.Rating, req.Id)
	if err != nil {
		return &pb.ReviewResponse{}, err
	}
	err = r.db.QueryRow(`SELECT 
	id, name, 
	description, 
	rating FROM review 
	WHERE id=$1 AND deleted_at IS NULL`, req.Id).Scan(
		&review.Id, &review.Name, &review.Description, &review.Rating,
	)
	if err != nil {
		fmt.Println("error while getting review in update")
		return &pb.ReviewResponse{}, err
	}
	return &review, nil
}

// DeleteReview ...
func (r *reviewRepo) DeleteReview(req *pb.ReviewId) (*pb.Empty, error) {
	_, err := r.db.Exec(`UPDATE review SET deleted_at=NOW() WHERE deleted_at IS NULL  AND id=$1`, req.Id)
	if err != nil {
		return &pb.Empty{}, err
	}
	return &pb.Empty{}, nil
}
