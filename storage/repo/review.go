package repo

import (
	pb "review_service/genproto/review"
)

type ReviewStorageI interface {
	CreateReview(*pb.ReviewRequest) (*pb.ReviewResponse, error)
	GetReviewById(*pb.ReviewId) (*pb.Reviews, error)
	GetReviewPost(postId int64) (*pb.Reviews, error)
	GetReviewCustomer(ownerId int64) (*pb.Reviews, error)
	UpdateReview(*pb.ReviewUp) (*pb.ReviewResponse, error)
	DeleteReview(*pb.ReviewId) (*pb.Empty, error)
}