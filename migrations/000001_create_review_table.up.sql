CREATE TABLE IF NOT EXISTS review(
    id serial primary key,
    post_id int,
    owner_id int,
    name varchar(50),
    rating INTEGER NOT NULL CHECK(rating >= 1 AND rating <= 5),
    description text,
    created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    deleted_at TIMESTAMP
);